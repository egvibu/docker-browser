import time
import unittest
import xmlrunner
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os


options = webdriver.ChromeOptions()
capabilities = options.to_capabilities()
driver = webdriver.Remote( command_executor='http://chrome:4444/wd/hub', desired_capabilities=capabilities)


class VdsinaEnter(unittest.TestCase):

    def test_entering_vdsina(self):

        driver.get("https://www.vdsina.ru")
        time.sleep(5)

        driver.find_element_by_xpath('/html/body/div[1]/div[1]/header/div/div/div[3]/div/ul/li[1]/a/span').click()
        login = driver.find_element_by_xpath('//*[@id="pop-up-auth"]/div/form/div/div[1]/div[1]/div/input')
        login.send_keys(os.environ["CIVAR_VDSINA_LOGIN"])
        password = driver.find_element_by_xpath('//*[@id="pop-up-auth"]/div/form/div/div[1]/div[2]/div/input')
        password.send_keys(os.environ["CIVAR_VDSINA_PASSWORD"])
        driver.save_screenshot("screen1.png")
        password.send_keys(Keys.ENTER)
        time.sleep(5)
        driver.save_screenshot("screen2.png")

        driver.find_element_by_xpath('/html/body/div[1]/div[1]/header/div/div/div[3]/div/ul/li[1]/a').click()
        time.sleep(5)

        driver.switch_to.window(driver.window_handles[1])

        '''
        fin = open('balance.txt', 'w')
        fin.write(driver.find_element_by_xpath('//*[@id="app"]/div[1]/div[2]/div/div[1]/div/div[1]/div/span[2]/span').text)
        fin.close()
        '''
        with open('balance.txt', 'w') as fin:
            bal = driver.find_element_by_xpath('//*[@id="app"]/div[1]/div[2]/div/div[1]/div/div[1]/div/span[2]/span').text.split()
            b1 = ''
            if len(bal) > 1:
                for elem in bal:
                   b1 += elem
            fin.write(b1)
            fin.close()
        assert "https://cp.vdsina.ru/vds/list" == driver.current_url


if __name__ == '__main__':
    with open('report_selen.xml', 'wb') as output:
        unittest.main(
            testRunner=xmlrunner.XMLTestRunner(output=output),
            failfast=False, buffer=False, catchbreak=False)